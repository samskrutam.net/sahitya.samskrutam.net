---
title: "Distinction_between_sentiment_emtion"
date: 2020-07-25T17:31:39-07:00
draft: false
weight: 40
---

## The Distinction between Sentiment and Emotional fervour

As we know, the Natya Sastra is purposefully kept in the interactive format of question and answer. This is the easiest way to attain knowledge and to impart knowledge. Here very eminent and knowledgeable sages ask questions and Bharat Muni answers them. The sages are also very eager and being scholars themselves, the level of questions is high so that profound content can be achieved. For example in the sixth chapter of the Natya Sastra sages ask finer questions like “How do these Rasas get their Rasatva?” This is a question put by a scholar who is eager to know the ultimate of the art and RASA. This is not only because Rasa in Indian art got prime importance but also that the sages ask about various aspects of rasa, which are mentioned in the book as quoted below.


### The query of the Sages

1-2. After listening to the explanation of the preliminary rites the venerable sages asked Bharata Muni “We have five questions to be asked. It behoves you to clarify them.” What have been recounted as Rasas in the dramatic art by those who are proficient in that art should be explained. How do these Rasas get their Rasatva?

3.	What have been mentioned as Bhavas how do they make us feel the various emotions? Kindly explain the terms Samgraha (Synoptic gist), Karika (Mnemonical Verse) and Nirukta (Etymological derivation). (The Natya Sastra, Of BHARATAMUNI by BOS Chapter 6 sloka 1-3 page no. 70)


The questions of the sages are also very important. In clarification, Bharata Muni says that, “It is impossible to reach the entire limit of the art of dance and drama.” Also in the next sloka Bharata Muni again confirms that his Sutras (Aphorisms) are limited in number to cover the dramatic depth of the topics of Rasa and Bhava. This means that he considers art has no boundaries or any limits.

### Bharata begins to answer


4-5. On hearing the request of the sages, Bharata resumed his explanation based on the distinction between Rasa and Bhava “O saints, I shall expound in the due order the terms Samgraha, Karika and Nirukta.”

6-7. It is impossible to reach the entire limit of the art of dance and drama. Why? Since the lores are many and the arts and craft are infinite. Even one branch of the Vast ocean of knowledge cannot be completed. Then where is the question of mastering the principles and meanings of all the Bhavas? (The Natya Sastra, of BHARATAMUNI by BOS Chapter 6 sloka 4-7 page no. 70)


It is not difficult to believe what Bharata Muni is saying, that there is scope for the inferences for comprehension. This proves that

Bharata Natya Sastra is also flexible and has ample scope for the creative mind and heart. So the Sastra is not forcing anything on

anybody but it is simply shows the milestone, identifies the goal for

the performers, shows a path and explains how to prepare oneself

to be able to walk on that path. The aim is to give the examples and


carve a path for the actor to walk with ample creativity. This is a	1/12

system in itself. How it is a system in itself will be unfolded through

further analysis of the Sastra. One important thing to note here is

that this forms the foundation of the system within Sastra.


When the Natya Sastra is referred, it means this is information


about; the Samgraha (collection) of everything like Rasas, Bhavas, . 1/13 Abhinaya (gesticulatory representation) Dharmi (Rehearsed practice), Vrtti (Style), Pravrtti, (Action), Siddhi (Achievement), Svaras (notes), Atodya (instrumental music), Gana (song), and Ranga (the stage), etc. Thus, the Natya Sastra is not only conceptually different but has also its unique definition of the theatre.

 



In the modern world, all the concepts have gone to its minutest details i.e. from mini to micro levels. However, the Natya Sastra is a torch light concept, which means that practically whatever required to do a good theatre is covered under the Natya Sastra. Literature, acting, dance, song, music, production and the theatre and stage are covered under this Sastra. Unique features of the Natya Sastra are Dharmi (Rehearsed practice), Vrtti (Style), Pravrtti, (Action), Siddhi (Achievement. Thus, it suggests that not merely external requirements but internal needs and intangible assets in this form too are required for theatre. In other words, this is a process of not



