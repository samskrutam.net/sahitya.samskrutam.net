---
title: "08 जना जगत्या"
date: 2020-08-01T00:00:50-07:00
draft: false
weight: 27

---
<div class="shloka">
<div class="shloka_first">

जना जगत्या जगदीश शश्वत्
 <br/>
   त्वां  रधिकाजारमुदाहरन्ति ॥
</div> 
<div class="shloka_second">
निन्दन्तु  लोका यदि वा स्तुवन्तु

 <br/>
प्राणेश्वरीं  त्वां  न परित्यजामि ॥
</div>

</div>
